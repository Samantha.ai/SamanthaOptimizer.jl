# SamanthaOptimizer.jl

[![pipeline status](https://gitlab.com/Samantha.ai/SamanthaOptimizer.jl/badges/master/pipeline.svg)](https://gitlab.com/Samantha.ai/SamanthaOptimizer.jl/commits/master)

Optimizer built on top of Samantha.jl and BlackBoxOptim.jl, which allows the optimization of both model shape and model parameters.

## Installation
Install a supported version of Julia (currently 0.6 or greater) on a supported OS.  
```
Pkg.clone("https://gitlab.com/Samantha.ai/SamanthaOptimizer.jl")
```

## License
SamanthaOptimizer is licensed under the MIT "Expat" license. Please see LICENSE.md for the full license terms.
