#addprocs(1)#Sys.CPU_CORES)

@everywhere begin
  using SamanthaOptimizer
  global const PATCH_CLAMP_ENABLE = false
  using Samantha
  using OnlineStats
  ccall(:jl_exit_on_sigint, Void, (Cint,), 0)
end

DATA_PATH = "/samantha/data/simulations"
SIM_PATH = joinpath(DATA_PATH, "simulation_" * string(now()))

function main()
  mkdir(SIM_PATH)
  seed_agent = include("seed_agent.jl")
  objective_path = joinpath(pwd(), "objective.jl")
  run_server(SIM_PATH, seed_agent, objective_path)
end
main()
