seed_agent = Agent()
iface = Dict{Symbol,String}()

n_in = GenericNeurons(16)
iface[:input] = n_in_l = addnode!(seed_agent, n_in)
n_exc = GenericNeurons(32)
iface[:output] = n_exc_l = addnode!(seed_agent, n_exc)
n_inh = GenericNeurons(16)
iface[:inhib] = n_inh_l = addnode!(seed_agent, n_inh)

s_in_exc = GenericSynapses(16, 32)
s_in_exc_l = addnode!(seed_agent, s_in_exc)
addedge!(seed_agent, s_in_exc_l, (
  (n_in_l, :input),
  (n_exc_l, :output)
))
s_exc_inh = GenericSynapses(32, 16)
s_exc_inh_l = addnode!(seed_agent, s_exc_inh)
addedge!(seed_agent, s_exc_inh_l, (
  (n_exc_l, :input),
  (n_inh_l, :output)
))
s_inh_exc = GenericSynapses(16, 32)
s_inh_exc_l = addnode!(seed_agent, s_inh_exc)
addedge!(seed_agent, s_inh_exc_l, (
  (n_inh_l, :input),
  (n_exc_l, :output)
))

SeedAgentWrapper(seed_agent, iface)
