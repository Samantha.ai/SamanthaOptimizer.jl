using Levenshtein

function generate_objective(strs::Vector{String})
  Objective((
    :levenshtein,
    :intertrial_silence,
    :intratrial_activity,
    :rung_bell,
    :completion_time,
    :agent_runtime
  ), function opt_func(params::Vector{Float64})
    # Training Phase
    for str in strs
      # Present string
      for char in str
        _present_char(input_N, char, 10f0)
        run!(agent)
      end

      # Present newline (Go)
      _present_char(input_N, '\n', 10f0)
      run!(agent)

      # Run until bell rung or timeout
      i = 1
      while i < 10*length(str)
        run!(agent)
        # TODO: Collect outputs
        if _get_char(input_N) == '\n'
          break
        end
      end
    end
    
    # Evaluate agent runtime
    _start_time = timens()
    run!(agent)
    agent_runtime = timens() - _start_time

    # TODO: Testing Phase
    for str in strs
    end

    return (
      agent_runtime
    )
  end)
end

function _present_char(n::GenericNeurons, char::Char, amplitude::Float32)
  n.state.I[Int(char)] = amplitude
end
