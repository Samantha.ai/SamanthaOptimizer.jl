#= TODO: Text Generation Objective
Needed:
Converters for char-to-spikes and spikes-to-char

Objectives:
Echo order/accuracy (Levenshtein distance?)
Silence during non-output phase
Activity during output phase
Agent run duration
=#

(seed_agent, iface) -> begin
  # Generate input sequences
  seqs = Vector{Bool}[]
  for i in 1:4
    push!(seqs, rand(Bool, 16))
  end

  Objective((
    :objective_silence,
    :objective_activity,
    :objective_variance
  ), function opt_func(params)
    agent = deepcopy(seed_agent)
    oldparams = params
    params = deepcopy(params)
    try
      setoptparams!(agent, params)
    catch err
      info(length(getoptparams(seed_agent)[1]))
      for cont in values(agent.nodes)
        info(typeof(root(cont)))
      end
      rethrow(err)
    end

    n_in_l = iface[:input]
    n_exc_l = iface[:output]
    n_inh_l = iface[:inhib]

    # Training Phase
    for t in 1:10
      for seq in seqs
        # Allow agent to settle
        for i in 1:10
          run!(agent)
        end

        # Apply sequence
        for i in 1:10
          root(agent.nodes[n_in_l]).state.I .= 20f0 .* seq
          run!(agent)
        end
      end
    end

    #= Objectives
    Silence during non-input
    Activity during input
    Consistency of chosen outputs
    TODO: Sharp partitioning of chosen outputs
    =#

    # Testing Phase
    cm_silence = Series(CountMap(Bool))
    cm_activity = Series(CountMap(Bool))
    fm_seq = [fit!(Series(FitMultinomial(32)), fill(1.0, 32)) for idx in 1:length(seqs)]
    for (idx,seq) in enumerate(seqs)
      # Allow agent to settle
      for i in 1:50
        run!(agent)
      end

      # Check for silence
      for i in 1:30
        run!(agent)
        fit!(cm_silence, root(agent.nodes[n_exc_l]).state.F)
      end

      # Apply sequence
      for i in 1:10
        root(agent.nodes[n_in_l]).state.I .= 20f0 .* seq
        run!(agent)
        fit!(cm_activity, root(agent.nodes[n_exc_l]).state.F)
        fit!(fm_seq[idx], root(agent.nodes[n_exc_l]).state.F)
      end
    end

    # Return objective evaluations
    prob_silence = probs(stats(cm_silence)[1], [false,true])[2]
    dist_activity = abs(1/length(seqs) - probs(stats(cm_activity)[1], [false,true])[2])
    var_output = sum([var(last(value(first(stats(fm))))) for fm in fm_seq])
    #=
    info(prob_silence, " | ", dist_activity, " | ", var_output)
    if isnan(var_output)
      vals = Float64[]
      for fm in fm_seq
        push!(vals, var(last(value(first(stats(fm))))))
      end
      info(vals)
      error("NaN")
    end
    =#
    return (prob_silence, dist_activity, var_output)
  end)
end
