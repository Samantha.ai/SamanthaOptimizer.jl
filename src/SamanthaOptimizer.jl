module SamanthaOptimizer

using Samantha
using ChimeraEngine
using UUIDs
#using Distributions

include("operators.jl")
include("optparams.jl")
include("agent-transformer.jl")

end
