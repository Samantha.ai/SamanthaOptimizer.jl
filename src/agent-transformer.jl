### Imports ###

import ChimeraEngine: InterfaceView
import ChimeraEngine: generate_model, apply_params, get_params, isinput, isoutput

### Exports ###

export AgentTransformer, WrappedAgent

### Types ###

struct AgentTransformer
  operators::Vector{EvolutionOperator}
end
AgentTransformer() = AgentTransformer(EvolutionOperator[])
ChimeraEngine.register!(:model_transformer, AgentTransformer)

struct WrappedAgent
  agent::Agent
  iface_map::Dict{Symbol,Base.UUID}
end
WrappedAgent(agent) = WrappedAgent(agent, Dict{Symbol,Base.UUID}())

### Methods ###

function ChimeraEngine.generate_model(at::AgentTransformer, iface_defs)
  model = WrappedAgent(Agent())
  agent = model.agent
  input_iface_map = Dict{Symbol,UUID}()
  output_iface_map = Dict{Symbol,UUID}()
  layers = UUID[]
  for output in filter(isoutput, iface_defs)
    if output.typ <: AbstractArray && output.meta isa ChimeraEngine.ArrayInterface
      # Create PatchClamp
      sz = output.meta.size
      pc = PatchClamp(:output, eltype(output.typ), sz...)
      pcl = addnode!(agent, pc)
      output_iface_map[output.name] = pcl

      # Create and attach GenericLayer to PatchClamp
      layer = GenericLayer(first(sz))
      ll = addnode!(agent, layer)
      addedge!(agent, pcl, ll, :input)
      push!(layers, ll)

      # Add mapping
      model.iface_map[output.name] = pcl
    else
      error("Output of type $output not supported")
    end
  end
  for input in filter(isinput, iface_defs)
    if input.typ <: AbstractArray && input.meta isa ChimeraEngine.ArrayInterface
      # Create PatchClamp
      sz = input.meta.size
      pc = PatchClamp(:input, eltype(input.typ), sz...)
      pcl = addnode!(agent, pc)
      input_iface_map[input.name] = pcl

      # Attach all GenericLayers to PatchClamp
      for ll in layers
        addedge!(agent, ll, pcl, :input)
      end

      # Add mapping
      model.iface_map[input.name] = pcl
    else
      error("Input of type $input not supported")
    end
  end
  params, bounds = getoptparams(agent)
  return model, (params, bounds), nothing
end
function generate_model(at::AgentTransformer, iface_defs, model::WrappedAgent)
  agent = model.agent
  #= FIXME
  constraints = Constraint[
    Constraint(
      NodeTypePattern(PatchClamp),
      NodeAddRestriction()
    ),
    Constraint(
      NodeTypePattern(PatchClamp),
      NodeDeleteRestriction()
    ),
  ]
  opers = filter(oper->isviable(agent, oper, constraints), at.operators)
  # FIXME: idx = decide(Int, bounds=1:length(opers))
  oper = rand(opers)
  agent, transform = apply_operator(agent, oper, constraints)
  =#
  transform = nothing
  params, bounds = getoptparams(agent)
  return WrappedAgent(agent, model.iface_map), (params, bounds), transform
end
get_params(model::WrappedAgent) =
  getoptparams(model.agent)
function apply_params(model::WrappedAgent, params::Vector{Float64})
  reinit!(model.agent)
  setoptparams!(model.agent, params)
  return model
end
function Base.getindex(iv::InterfaceView{WrappedAgent})
  agent = iv.model.agent
  id = iv.model.iface_map[iv.sym]
  pc = root(agent.nodes[id])
  return pc.buffer
end
function Base.setindex!(iv::InterfaceView{WrappedAgent}, value::AbstractVector)
  agent = iv.model.agent
  id = iv.model.iface_map[iv.sym]
  pc = root(agent.nodes[id])
  pc.buffer .= value
end
ChimeraEngine.ce_run_model(model::WrappedAgent) =
  run!(model.agent)
