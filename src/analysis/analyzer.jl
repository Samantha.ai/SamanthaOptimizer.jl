using Samantha
using DataFrames
using JuliaDB
using OnlineStats
using JLD2

include(joinpath("..", "types.jl"))
include(joinpath("..", "optparams.jl"))
include(joinpath("..", "patch_clamp.jl"))
global const PATCH_CLAMP_ENABLE[] = true

struct SimulationResult
  dir_name::String
  parent::String
  seed_agent::Agent
  candidates::NextTable
end

function load_simulation(sim_path::String)
  results = SimulationResult[]

  # Load all simulation results
  for (root,dirs,files) in walkdir(sim_path)
    for dir in dirs
      dir_name = joinpath(root, dir)
      cd(dir_name) do
        parent = readline("parent.txt")
        #seed_agent = load("agent.jld", "nodes")
        seed_agent = jldopen("agent.jld2", "r") do f
          f["agent"]
        end
        candidates = loadtable("candidates.csv")
        result = SimulationResult(dir_name, parent, seed_agent, candidates)
        push!(results, result)
      end
    end
  end

  results
end

function simulate(seed::SeedAgentWrapper, objective_path)
  # Load objective
  obj_gen = include(objective_path)
  obj = Base.invokelatest(obj_gen, seed.agent, seed.interface)

  # Run simulation
  params = getoptparams(seed.agent)[1]
  results = Base.invokelatest(obj.func, params)
  clamp_data = copy(PATCH_CLAMPS)
  empty!(PATCH_CLAMPS)

  # Return results and patch clamp data
  return ((results, obj.keys), clamp_data)
end

function objective_stats(results)
  obj_keys = (filter(k->startswith(string(k), "objective"), keys(first(first(results).candidates)))...,)
  nobjs = length(obj_keys)
  mean_stats = [reduce(nobjs*Mean(), res.candidates; select=obj_keys) for res in results]
  var_stats = [reduce(nobjs*Variance(), res.candidates; select=obj_keys) for res in results]
end

# TODO: get_lineage => Takes in a results vector and starting point, and returns a lineage vector,
# moving from the starting point towards its root

#=
function lineages(sims::Vector{SimulationResult})
  sims = copy(sims)
  lins = SimulationBranch[]
  while length(sims) > 0
    sim = pop!(sims)
    if sim.parent == ""
      branch = SimulationBranch(sim)
      push!(lins, branch)
    else
      # FIXME: Try to find in sims
      idx = findfirst(_sim->_sim.dir_name==sim.parent, sims)
      if idx == 0
        # FIXME: If not in sims, locate in lins
      else
        
      end
    end
  end
  lins
end
=#
