function d3_pane()
  scope = Scope()
  import!(scope, ["https://cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.min.js"])

  sketch = @js function (d3)
	ul = d3.selectAll("div#d3div").insert("ul");
	ul = ul.selectAll("li").data([1,2,3]);
	ul.enter().append("li").text(function (d) d+5 end);
	ul.exit().remove();
  end
  onimport(scope, sketch)

  scope.dom = dom"div#d3div"()

  scope
end
d3_app()
