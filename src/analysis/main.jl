include("analyzer.jl")
include("plotting.jl")

function main()
  #= TODO
  Set watcher for new data, load on directory creation after small delay
  Load all current data
  Render scatter+regression of objective averages over time
  =#
  seed = include("../../examples/lateral_inhibition/seed_agent.jl")
  obj_path = "../../examples/lateral_inhibition/objective.jl"
  spikefilter = data->filter((k,v)->endswith(string(k), "_F"), data)
  bandfilter = data->filter((k,v)->!endswith(string(k), "_F"), data)
  plt = plot_candidate(seed, obj_path; spikefilter=spikefilter, bandfilter=bandfilter)
  # TODO: Save plot to file
  #savefig(plt, id)
end
main()
