using Plots, PlotThemes
gr(); theme(:solarized)

function plot_candidate(seed::SeedAgentWrapper, objective_path; spikefilter=data->similar(data,0), bandfilter=data->similar(data,0))
  res, clamp_data = simulate(seed, objective_path)
  results, obj_keys = res

  # Filter data components
  spikes, bands = spikefilter(clamp_data), bandfilter(clamp_data)

  plt = plot(;size=(800,600))

  # Plot spiketrains
  plotspikes!(plt, spikes)

  # Overlay bands of interest
  #plotbands!(plt, bands)

  plt
end

function plotspikes!(plt, data)
  ticks = tuple(collect(1:length(data)), collect(keys(data)))
  for (idx,key) in enumerate(collect(keys(data)))
    scatter!(plt, [any(di) ? idx : NaN for di in data[key]]; markersize=8, label="")
  end
  plot!(plt; title="Agent Spiketrain", xlabel="Time", ylabel="Neurons", yticks=ticks)
  plt
end

# FIXME
function plotbands!(plt, bands)
  plot!(plt, 50:60, 10.5.*ones(11); fillrange=0,
									fillalpha=0.2,
									fillcolor=:green,
									linecolor=nothing,
									label="Active",
									legend=:best)
end
