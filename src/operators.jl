import Base: match

### Exports ###

export EvolutionOperator, MutationOperator, RecombinationOperator
export CloneNodeMutation, DeleteNodeMutation, AddNodeMutation, LearnAlgorithmMutation
export DeleteEdgeMutation, AddEdgeMutation

export Pattern, NodePattern, EdgePattern
export NodeIDPattern, NodeTypePattern, EdgeAnyPattern, EdgeExactPattern

export Constraint

### Types ###

abstract type Pattern end
abstract type NodePattern <: Pattern end
abstract type EdgePattern <: Pattern end

struct NodeIDPattern <: NodePattern
  nodeid::UUID
end
struct NodeTypePattern <: NodePattern
  nodetype::Type
end

struct EdgeAnyPattern{ST,DT} <: Pattern
  srcnodetype::Type{ST}
  dstnodetype::Type{DT}
  op::Union{Symbol,Nothing}
end
struct EdgeExactPattern <: EdgePattern
  srcid::UUID
  dstid::UUID
  op::Symbol
end
EdgeExactPattern(edge::Tuple{UUID,UUID,Symbol}) = EdgeExactPattern(edge...)

# TODO: Use Pattern for Operator matching
abstract type EvolutionOperator end
abstract type MutationOperator <: EvolutionOperator end
abstract type RecombinationOperator <: EvolutionOperator end

struct CloneNodeMutation{T} <: MutationOperator
  nodetype::Type{T}
end
struct DeleteNodeMutation{T} <: MutationOperator
  nodetype::Type{T}
end
struct AddNodeMutation{N<:AbstractNode} <: MutationOperator
  node::N
end
struct LearnAlgorithmMutation <: MutationOperator
  algorithms::Vector
end

struct DeleteEdgeMutation{ST,DT} <: MutationOperator
  srcnodetype::Type{ST}
  dstnodetype::Type{DT}
  op::Union{Symbol,Nothing}
end
struct AddEdgeMutation{ST,DT,C} <: MutationOperator
  srcnodetype::Type{ST}
  dstnodetype::Type{DT}
  op::Symbol
  conf::C
end

abstract type Restriction end

"Prevents adding (or cloning) a node."
struct NodeAddRestriction <: Restriction end
"Prevents deleting a node."
struct NodeDeleteRestriction <: Restriction end
"Prevents modifying a node's mutable parameters or structure."
struct NodeModifyRestriction <: Restriction end

"Prevents adding an edge."
struct EdgeAddRestriction <: Restriction end
"Prevents deleting an edge."
struct EdgeDeleteRestriction <: Restriction end

"Defines a restriction to be placed when matched to a pattern."
struct Constraint{P<:Pattern,R<:Restriction}
  pattern::P
  restriction::R
end

### Methods ###

function node_filter(agent, nodetype, cons)
  filter(kv->root(kv[2]) isa nodetype, match(agent.nodes, cons))
end
function edge_filter(agent, srcnodetype, dstnodetype, op, cons)
  filter(edge->begin
    srcnode = root(agent.nodes[edge[1]])
    dstnode = root(agent.nodes[edge[2]])

    srcnode isa srcnodetype && \
    dstnode isa dstnodetype && \
    op !== nothing ? edge[3] === op : true
  end, match(agent.edges, cons))
end

function isviable(agent, op::AddNodeMutation, cons)
  # Find all constraints that match and restrict us
  _cons = filter(con->(con.pattern<:NodeTypePattern && con.pattern.nodetype<:op.nodetype)&&con.restriction<:NodeAddRestriction, cons)
  # If any found, no good!
  return length(_cons) == 0
end
function isviable(agent, op::DeleteNodeMutation, cons)
  # Do we have nodes we can delete?
  any(kv->typeof(root(kv[2]))<:op.nodetype, match(agent, op, cons))
end
function isviable(agent, op::CloneNodeMutation, cons)
  # FIXME: Filter on NodeAddRestriction
  # FIXME: Filter on EdgeAddRestriction
  # FIXME: match selects matching nodes
  # Filter out both nodes and edges that we can't add
  _agent = match(agent, op, cons)
  # If any nodes left, then we're good
  return length(_agent.nodes) > 0
end

function isviable(agent, op::AddEdgeMutation, cons)
  # FIXME: Filter on EdgeAddRestriction
  # FIXME: Filter out edges we don't match
  _agent = match(agent, op, cons)
  return length(_agent.edges) > 0
  hassrcnode = length(node_filter(agent, op.srcnodetype, cons)) > 0
  hasdstnode = length(node_filter(agent, op.dstnodetype, cons)) > 0
  return hassrcnode && hasdstnode
end
function isviable(agent, op::DeleteEdgeMutation, cons)
  # FIXME: Filter on EdgeDeleteRestriction
  length(edge_filter(agent, op.srcnodetype, op.dstnodetype, op.op, cons)) > 0
end

function isviable(agent, op::LearnAlgorithmMutation, cons)
  # FIXME: Filter on NodeModifyRestriction
  return length(match(agent.nodes, op, cons).nodes) > 0
  #any(kv->typeof(root(kv[2]))<:GenericSynapses, match(agent.nodes, op, cons))
end

function apply_operator(agent, op::CloneNodeMutation, cons)
  valid_ids = collect(keys(filter(kv->typeof(root(kv[2]))<:op.nodetype, agent.nodes)))
  id = rand(valid_ids)
  cloned_node = deepcopy(root(agent.nodes[id]))
  clone_id = addnode!(agent, cloned_node)
  orig_edges = filter(edge->edge[1]==id, agent.edges)
  deledges!(agent, clone_id)
  # FIXME: We need to pass in conf somehow
  for edge in orig_edges
    addedge!(agent, clone_id, edge[2], edge[3])
  end
  agent, nothing
end
function apply_operator(agent, op::DeleteNodeMutation, cons)
  valid_ids = collect(keys(filter(kv->typeof(root(kv[2]))<:op.nodetype, match(agent.nodes, cons))))
  id = rand(valid_ids)
  # FIXME: This should be done by delnode! instead
  dead_edges = filter(edge->((edge[1]==id)||(edge[2]==id)), agent.edges)
  for edge in dead_edges
    deledge!(agent, edge)
  end
  delnode!(agent, id)
  agent, nothing
end
function apply_operator(agent, op::AddNodeMutation, cons)
  node = deepcopy(op.node)
  id = addnode!(agent, node)
  agent, nothing
end
function apply_operator(agent, op::LearnAlgorithmMutation, cons)
  valid_ids = collect(keys(filter(kv->typeof(root(kv[2]))<:GenericSynapses, match(agent.nodes, cons))))
  id = rand(valid_ids)
  algo = rand(op.algorithms)
  node = root(agent.nodes[id])
  new_node = GenericSynapses(node; learn=algo())
  # TODO: Make this more "elegant" (esp. for GPU arrays)
  new_cont = CPUContainer(new_node)
  agent.nodes[id] = new_cont
  agent, nothing
end
function apply_operator(agent, op::DeleteEdgeMutation, cons)
  edges = edge_filter(agent, op.srcnodetype, op.dstnodetype, op.op, cons)
  edge = rand(edges)
  deledge!(agent, edge)
  return agent, nothing
end
function apply_operator(agent, op::AddEdgeMutation, cons)
  src_nodes = collect(keys(node_filter(agent, op.srcnodetype, cons)))
  dst_nodes = collect(keys(node_filter(agent, op.dstnodetype, cons)))
  src_node = rand(src_nodes)
  dst_node = rand(dst_nodes)
  addedge!(agent, src_node, dst_node, op.op, op.conf)
  return agent, nothing
end

"""
Generates a list of constrains which fully preserves the structure of the specified agent.
"""
# FIXME
function generate_constraints(agent::Agent)
  cons = Pattern[]
  for id in keys(agent.nodes)
    push!(cons, NodeIDPattern(id))
  end
  for edge in agent.edges
    push!(cons, EdgeExactPattern(edge))
  end
  cons
end

# Top-level match
function match(agent::Agent, op, cons::Vector{<:Constraint})
  _agent = deepcopy(agent)
  for con in cons
    match!(_agent.nodes, op, con)
  end
  return _agent
end
# Default on non-match
match!(agent::Agent, op, con::Constraint{<:Any,<:Any}) = ()

function match!(agent::Agent, op::AddNodeMutation, con::Constraint{<:NodePattern,<:NodeAddRestriction})
  for (uuid,node) in agent.nodes
    if match((uuid,node), con.pattern)
      delnode!(agent, uuid)
    end
  end
end
function match!(agent::Agent, op::DeleteNodeMutation, con::Constraint{<:NodePattern,<:NodeDeleteRestriction})
  for (uuid,node) in agent.nodes
    if match((uuid,node), con.pattern)
      delnode!(agent, uuid)
    end
  end
end

match(node::Tuple{UUID,C}, pattern::NodePattern) where C<:AbstractContainer =
  match((node[1],root(node[2])), pattern)
match(node::Tuple{UUID,N}, pattern::NodeTypePattern) where N<:AbstractNode =
  N <: pattern.nodetype
match(node::Tuple{UUID,N}, pattern::NodeIDPattern) where N<:AbstractNode =
  node[1] == pattern.nodeid

function match!(agent::Agent, op::AddEdgeMutation, con::Constraint{<:EdgePattern,<:EdgeAddRestriction})
  for edge in agent.edges
    if match(edge, con.pattern)
      deledge!(agent, edge)
    end
  end
end
function match!(agent::Agent, op::DeleteEdgeMutation, con::Constraint{<:EdgePattern,<:EdgeDeleteRestriction})
  for edge in agent.edges
    if match(edge, con.pattern)
      deledge!(agent, edge)
    end
  end
end

match(edge::Tuple{UUID,UUID,Symbol}, pattern::EdgeExactPattern) =
  edge == (pattern.srcid, pattern.dstid, pattern.op)
# FIXME: Requires nodes as well
#match(edge::Tuple{UUID,UUID,Symbol}, pattern::EdgeAnyPattern) =
#  (edge[1] == pattern.srcid) || (edge[2] == pattern.dstid) || (pattern.op !== nothing ? edge[3] == pattern.op : false)

#=
function match(nodes::Dict{UUID,AbstractContainer}, cons::Vector{<:Pattern})
  _nodes = copy(nodes)
  _cons = filter(con->con isa NodePattern, cons)
  for con in _cons
    _nodes = match(_nodes, con)
  end
  _nodes
end

function match(edges::Vector{Tuple{UUID,UUID,Symbol}}, cons::Vector{<:Pattern})
  _edges = copy(edges)
  _cons = filter(con->con isa EdgePattern, cons)
  for con in _cons
    _edges = match(_edges, con)
  end
  _edges
end


match(nodes::Dict{UUID,AbstractContainer}, con::NodeIDPattern) =
  filter(kv->!(kv[1]==con.nodeid), nodes)
match(nodes::Dict{UUID,AbstractContainer}, con::NodeTypePattern) =
  filter(kv->!(typeof(root(kv[2]))<:con.nodetype), nodes)

match(edges::Vector{Tuple{UUID,UUID,Symbol}}, con::EdgeExactPattern) =
  filter(edge->edge!=(con.srcid,con.dstid,con.op), edges)
=#
