export getoptparams, setoptparams!

function getoptparams(agent::Agent; params=Float64[], bounds=Tuple{Float64,Float64}[])
  sorted_keys = sort(collect(keys(agent.nodes)))
  for key in sorted_keys
    getoptparams(agent.nodes[key]; params=params, bounds=bounds)
  end
  (params, bounds)
end
function setoptparams!(agent::Agent, params)
  sorted_keys = sort(collect(keys(agent.nodes)))
  for key in sorted_keys
    setoptparams!(agent.nodes[key], params)
  end
end

getoptparams(cont::AbstractContainer; kwargs...) = getoptparams(root(cont); kwargs...)
setoptparams!(cont::AbstractContainer, params) = setoptparams!(root(cont), params)

function getoptparams(layer::GenericLayer; params=nothing, bounds=nothing)
  getoptparams(layer.synapses; params=params, bounds=bounds)
  getoptparams(layer.neurons; params=params, bounds=bounds)
end
function setoptparams!(layer::GenericLayer, params)
  setoptparams!(layer.synapses, params)
  setoptparams!(layer.neurons, params)
end

function getoptparams(neurons::GenericNeurons; params=nothing, bounds=nothing)
  push!(params, neurons.conf.a); push!(bounds, (0.0, 1.0))
  push!(params, neurons.conf.b); push!(bounds, (0.0, 1.0))
  push!(params, neurons.conf.c); push!(bounds, (-100.0, -30.0))
  push!(params, neurons.conf.d); push!(bounds, (0.0, 10.0))
  push!(params, neurons.conf.thresh); push!(bounds, (-30.0, 50.0))
  push!(params, neurons.conf.θRate); push!(bounds, (0.0, 1.0))
  push!(params, neurons.conf.traceRate); push!(bounds, (0.0, 1.0))
  push!(params, neurons.conf.boostRate); push!(bounds, (0.0, 1.0))
end
function setoptparams!(neurons::GenericNeurons, params)
  neurons.conf.a = popfirst!(params)
  neurons.conf.b = popfirst!(params)
  neurons.conf.c = popfirst!(params)
  neurons.conf.d = popfirst!(params)
  neurons.conf.thresh = popfirst!(params)
  neurons.conf.θRate = popfirst!(params)
  neurons.conf.traceRate = popfirst!(params)
  neurons.conf.boostRate = popfirst!(params)
end

function getoptparams(synapses::GenericSynapses; params=nothing, bounds=nothing)
  push!(params, synapses.outputMask); push!(bounds, (-100.0, 100.0))
  for conn in sort(synapses.conns; by=conn->conn.uuid)
    getoptparams(conn.data; params=params, bounds=bounds)
  end
end
function setoptparams!(synapses::GenericSynapses, params)
  synapses.outputMask = popfirst!(params)
  for conn in sort(synapses.conns; by=conn->conn.uuid)
    setoptparams!(conn.data, params)
  end
end

function getoptparams(syni::SynapticInput; params=nothing, bounds=nothing)
  push!(params, syni.condRate); push!(bounds, (0.0, 1.0))
  push!(params, syni.traceRate); push!(bounds, (0.0, 1.0))
  getoptparams(syni.frontend; params=params, bounds=bounds)
  getoptparams(syni.learn; params=params, bounds=bounds)
  getoptparams(syni.modulator; params=params, bounds=bounds)
end
function setoptparams!(syni::SynapticInput, params)
  syni.condRate = popfirst!(params)
  syni.traceRate = popfirst!(params)
  setoptparams!(syni.frontend, params)
  setoptparams!(syni.learn, params)
  setoptparams!(syni.modulator, params)
end

function getoptparams(frontend::GenericFrontend; params=nothing, bounds=nothing)
  for delay in frontend.ID
    push!(params, delay); push!(bounds, (0, frontend.delayLength-1))
  end
end
function setoptparams!(frontend::GenericFrontend, params)
  for idx in axes(frontend.ID, 1)
    frontend.ID[idx] = clamp(round(Int, popfirst!(params)), 0, frontend.delayLength-1)
  end
end

function getoptparams(learn::SymmetricRuleLearn; params=nothing, bounds=nothing)
  push!(params, learn.α_pre); push!(bounds, (-1, 1))
  push!(params, learn.α_post); push!(bounds, (-1, 1))
  push!(params, learn.xtar); push!(bounds, (-1, 1))
  push!(params, learn.Wmax); push!(bounds, (-100, 100))
  push!(params, learn.μ); push!(bounds, (-1, 1))
end
function setoptparams!(learn::SymmetricRuleLearn, params)
  learn.α_pre = popfirst!(params)
  learn.α_post = popfirst!(params)
  learn.xtar = popfirst!(params)
  learn.Wmax = popfirst!(params)
  learn.μ = round(Int, popfirst!(params))
end

function getoptparams(learn::HebbianDecayLearn; params=nothing, bounds=nothing)
  push!(params, learn.α); push!(bounds, (-1, 1))
  push!(params, learn.β); push!(bounds, (-1, 1))
  push!(params, learn.Wmax); push!(bounds, (-100, 100))
end
function setoptparams!(learn::HebbianDecayLearn, params)
  learn.α = popfirst!(params)
  learn.β = popfirst!(params)
  learn.Wmax = popfirst!(params)
end

getoptparams(rm::RewardModulator; params=nothing, bounds=nothing) = ()
setoptparams!(rm::RewardModulator, params) = ()

getoptparams(pc::PatchClamp; params=nothing, bounds=nothing) = ()
setoptparams!(pc::PatchClamp, params) = ()

getoptparams(::Nothing; params=nothing, bounds=nothing) = ()
setoptparams!(::Nothing, params) = ()

#=
@generated function getoptparams(x; params=Float64[], bounds=Tuple{Float64,Float64}[])
  ex = Expr(:block)
  push!(ex.args, :(@info "Getting $($x)"))
  for field in fieldnames(x)
    ft = fieldtype(x, field)
    if ft <: Number
      push!(ex.args, :(push!(params, x.$field); push!(bounds, (0.0, 0.0))))#(-Inf, Inf))))
    elseif ft <: AbstractArray
      push!(ex.args, quote
        for i in x.$field
          push!(params, i); push!(bounds, (0.0, 0.0))#(-Inf, Inf))
        end
      end)
    else
      push!(ex.args, :(getoptparams(x.$field; params=params, bounds=bounds)))
    end
  end
  ex
end
@generated function setoptparams!(x, params)
  ex = Expr(:block)
  push!(ex.args, :(@info "Setting $($x)"))
  for field in fieldnames(x)
    ft = fieldtype(x, field)
    if ft <: Number
      push!(ex.args, :(x.$field = popfirst!(params)))
    elseif ft <: AbstractArray
      push!(ex.args, quote
        for idx in axes(x.$field, 1)
          x.$field[idx] = popfirst!(params)
        end
      end)
    else
      push!(ex.args, :(setoptparams!(x.$field, params)))
    end
  end
  ex
end
=#
