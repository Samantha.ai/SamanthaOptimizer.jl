import Base: iterate, length

struct AgentNodeIter
  agent::Agent
  uuid::UUID
end
# TODO: state acts as config
iterate(iter::AgentNodeIter, state=nothing) = ((root(iter.agent[uuid]), IdDict()), state)

struct SynapseWeightMoments{Iter}
  iter::Iter
end
function iterate(iter::SynapseWeightMoments, state=nothing)
  if state === nothing
    next = iterate(iter.iter)
    state = ((
      mean = Mean(),
      var = Variance()
    ), next[2])
  else
    next = iterate(iter.iter, state[2:end])
  end
  next === nothing && return nothing

  node = next[1][1]
  moments = state[1]
  fit!(moments.mean, #=FIXME=#)
  fit!(moments.var, #=FIXME=#)
  # FIXME: Other moments

  (next[1], state)
end

function demo(agent, uuid; maxiters=10)
  iter = AgentNodeIter(agent, uuid)
  iter = SynapseWeightMoments(iter)
  iter = take(iter, maxiters)

  x = nothing
  for y in iter x=y end
  return x
end
