import ChimeraEngine: InterfaceDefinition, ArrayInterface, generate_model, apply_params

@testset "Get/set optparams" begin
  agent = Agent()
  p1 = addnode!(agent, PatchClamp(Bool, 8))
  p2 = addnode!(agent, PatchClamp(Bool, 8))
  l1 = addnode!(agent, GenericLayer(8))
  addedge!(agent, l1, p1, :input)
  addedge!(agent, p2, l1, :input)

  old_params, old_bounds = getoptparams(agent)
  new_agent = deepcopy(agent)
  @test_broken agent == new_agent
  setoptparams!(new_agent, copy(old_params))
  new_params, new_bounds = getoptparams(new_agent)
  @test length(old_params) == length(new_params)
  @test length(old_bounds) == length(new_bounds)
  @test all(old_params .== new_params)
  @test all(old_bounds .== new_bounds)
  @test_broken agent == new_agent

  # Weed out issues with conversions to Int and negatives
  new_params .= -3.14
  setoptparams!(new_agent, new_params)
  run!(new_agent)
end

iface_defs = InterfaceDefinition[]
push!(iface_defs, InterfaceDefinition(:in1, :in, Vector{Bool}, ArrayInterface((8,))))
push!(iface_defs, InterfaceDefinition(:in2, :in, Vector{Bool}, ArrayInterface((16,))))
push!(iface_defs, InterfaceDefinition(:out1, :out, Vector{Bool}, ArrayInterface((8,))))
push!(iface_defs, InterfaceDefinition(:out2, :out, Vector{Bool}, ArrayInterface((16,))))
at = AgentTransformer(EvolutionOperator[])

@testset "Generate new model" begin
  model, (params, bounds), transform = generate_model(at, iface_defs)
  agent = model.agent
  @test length(agent.nodes) === 6
  @test params isa Vector{Float64}
  @test bounds isa Vector{Tuple{Float64,Float64}}
  @test transform === nothing
  model = apply_params(model, params)
  @test model isa WrappedAgent
end

@testset "Transform model" begin
  old_model, params, transform = generate_model(at, iface_defs)
  opers = [
    CloneNodeMutation(GenericLayer),
    DeleteNodeMutation(GenericLayer),
    AddNodeMutation(GenericLayer(8)),
    # FIXME: LearnAlgorithmMutation(#=FIXME=#),
    DeleteEdgeMutation(GenericLayer, PatchClamp, :input),
    AddEdgeMutation(GenericLayer, PatchClamp, :input, NamedTuple()),
  ]
  for oper in opers
    push!(at.operators, oper)
    @info oper
    model = deepcopy(old_model)
    model, params, transform = generate_model(at, iface_defs, model)
    @test_broken transform !== nothing
    if oper isa CloneNodeMutation
      @test length(model.nodes) === length(old_model.nodes) + 1
    elseif oper isa DeleteNodeMutation
      @test length(model.nodes) === length(old_model.nodes) - 1
    elseif oper isa AddNodeMutation
      @test length(model.nodes) === length(old_model.nodes) + 1
    elseif oper isa LearnAlgorithmMutation
      @test_broken false
    elseif oper isa DeleteEdgeMutation
      @test length(model.edges) === length(old_model.edges) - 1
    elseif oper isa AddEdgeMutation
      @test length(model.edges) === length(old_model.edges) + 1
    end
    model = apply_params(model, params)
    run!(model)
    empty!(at.operators)
  end
end
