using SamanthaOptimizer
using Samantha
using OnlineStats

(seed_agent, iface, config) -> begin
  config = merge(config, (
    target_μ = 0.2,
    target_σ = 0.0
  ))
  return Objective((
    :objective_mean,
    :objective_variance
  ), function opt_func(params)
    agent = deepcopy(seed_agent)
    params = copy(params)
    setoptparams!(agent, params)

    il = iface[:input]
    nl = iface[:neurons]
    sl = iface[:synapses]

    #= FIXME
    Generate:
      Input I -> One-hot input vector
    Train:
      Feed in input I
      Force-activate neurons N1 and N2
      Force-silence other neurons
      Run agent
    Test:
      Phase 1:
        Disable learning
        Feed in input I
        Run agent
        Objective: Neurons N1 and N2 activate, others remain silent
        Objective: Input I -> Neurons N1 and N2 weights are significantly large
      Phase 2:
        Enable learning
        Feed in input I
        Force-activate neuron N1
        Force-silence other neurons
        Run agent
        Objective: Input I -> Neuron N1 weight remains stable
        Objective: Input I -> Neuron N2 weight decreases???
      Phase 3:
        Feed in blank input
        Force-activate neuron N1
        Force-silence other neurons
        Run agent
        Objective: Input I -> Neuron N1 weight decreases???
    =#

    avg_μ = Mean()
    avg_σ = Variance()
    for t in 1:1000
      root(agent.nodes[nl]).state.I[1] = 20f0
      run!(agent)
      value = first(root(agent.nodes[nl]).state.F)
      fit!(avg_μ, value)
      fit!(avg_σ, value)
    end

    # Return objective evaluations
    μ = abs(value(avg_μ) - config.target_μ)
    σ = abs(value(avg_σ) - config.target_σ)
    return (μ, σ)
  end)
end
