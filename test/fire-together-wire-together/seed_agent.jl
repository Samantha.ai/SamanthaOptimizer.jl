seed_agent = Agent()
iface = Dict{Symbol,String}()

inp = AnyNode(zero(Bool, 8))
n1 = GenericNeurons(8)
s = GenericSynapses(8,8)
iface[:input] = addnode!(seed_agent, inp)
iface[:neurons] = addnode!(seed_agent, n)
iface[:synapses] = addnode!(seed_agent, s)
addedge!(seed_agent, iface[:synapses], (
  (iface[:input], :input),
  (iface[:neurons], :output),
))

SeedAgentWrapper(seed_agent, iface)
