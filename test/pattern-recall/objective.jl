using SamanthaOptimizer
using Samantha
using OnlineStats
using NamedTuples

(seed_agent, iface, config) -> begin
  config = merge(config, @NT(
    target_μ = 0.2,
    target_σ = 0.0
  ))
  return Objective((
    :objective_mean,
    :objective_variance
  ), function opt_func(params)
    agent = deepcopy(seed_agent)
    params = copy(params)
    setoptparams!(agent, params)

    nl = iface[:neurons]
    avg_μ = Mean()
    avg_σ = Variance()
    for t in 1:1000
      root(agent.nodes[nl]).state.I[1] = 20f0
      run!(agent)
      value = first(root(agent.nodes[nl]).state.F)
      fit!(avg_μ, value)
      fit!(avg_σ, value)
    end

    # Return objective evaluations
    μ = abs(value(avg_μ) - config.target_μ)
    σ = abs(value(avg_σ) - config.target_σ)
    return (μ, σ)
  end)
end
