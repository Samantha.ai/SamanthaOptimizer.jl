seed_agent = Agent()
iface = Dict{Symbol,String}()

n = GenericNeurons(8)
s = GenericSynapses(8,8)
iface[:neurons] = addnode!(seed_agent, n)
iface[:synapses] = addnode!(seed_agent, s)
addedge!(seed_agent, iface[:synapses], (
  (iface[:neurons], :input),
  (iface[:neurons], :output),
))

SeedAgentWrapper(seed_agent, iface)
