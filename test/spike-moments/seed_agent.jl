seed_agent = Agent()
iface = Dict{Symbol,String}()

iface[:neurons] = addnode!(seed_agent, GenericNeurons(1))

SeedAgentWrapper(seed_agent, iface)
