@testset "Spike Moments" begin
  server = test_simulate_prepare(@__DIR__)

  for target_μ in 0.0:0.2:1.0
    info("Testing target_μ at $target_μ")
    nt = (target_μ=target_μ,)
    test_simulate!(server, nt)
    frontier = SamanthaOptimizer.flatten_frontier(server.frontier)

    μ_all = map(cand->first(cand.params[:objective_mean]), frontier)
    μ_best = minimum(μ_all)
    info("μ_all: $μ_all")
    info("μ_best: $μ_best")
    @test μ_best < 0.05

    σ_all = map(cand->first(cand.params[:objective_variance]), frontier)
    σ_best = minimum(σ_all)
    info("σ_all: $σ_all")
    info("σ_best: $σ_best")
    @test σ_best < 0.05
  end
end
